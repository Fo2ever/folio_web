from flask import (
                Blueprint, 
                render_template, 
                request, redirect, 
                url_for
                )  

from flask_login import login_required



from forms import folios as folios_form
import models
from models import folios

bp = Blueprint("classes", __name__, url_prefix="/classes")

@bp.route('')
def index():
    return render_template("class/class.html")